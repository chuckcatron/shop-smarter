var app = app || {};

app.UserView = Backbone.View.extend({

	el: $("#user-wrap"),

	initialize: function(){
		this.render();
	},

	render: function(){
		$("[id*='-wrap']").hide();
		if(Parse.User.current()){
			this.$el.show();
			$("#list-userview").show();
		}
	},

	show: function(){
		this.render();
	}
});