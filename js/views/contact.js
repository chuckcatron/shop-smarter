var app = app || {};

app.ContactView = Backbone.View.extend({

	el: $("#contact-wrap"),

	events:{
		'click #btnSendComments' : 'sendComments'
	},

	initialize: function(){
		this.render();
	},

	render: function(){
		$("[id*='-wrap']").hide();
		this.$el.show();
	},

	sendComments: function(){
		var validator = $("#contact-wrap").kendoValidator().data("kendoValidator");
		if(validator.validate()){
			var name = $("#contactname").val().trim(),
				email = $("#contactemail").val().trim(),
				comments = $("#contactcomments").val().trim();

			this.sendEmail(name, email, comments);		
		}
	},

	show: function(){
		this.render();
	},

	sendEmail: function(name, email, comments){
		Parse.Cloud.run('sendMail', 
		    { "toEmail": "charles.catron@gmail.com",
		      "fromEmail": email,
		      "subject": "Shop Smarter Contact Form",
		      "comments": "Name: " + name + "\nComments: " + comments
		    }, 
		    {
		      success: function(result) {
		        alert(result); 
		        homeView.show();
	    	  },
		      error: function(error) {
		        alert(error.message);
		      }
			}
		);
	}
});