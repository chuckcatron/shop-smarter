var app = app || {};

app.LoginView = Backbone.View.extend({

	el: $("#login-wrap"),

	events: {
		'click #btnLogin': 'login',
	},

	initialize: function(){
		this.signinError = this.$("#signinError");
		this.username = this.$("#username");
		this.password = this.$("#password");
		this.render();
	},

	render: function(){
		this.$("#signinError").hide();
		$("[id*='-wrap']").hide();
		this.$el.show();
	},

	login: function(){
		var loginValidator = this.$el.kendoValidator().data("kendoValidator");
	    if(loginValidator.validate()){
			Parse.User.logIn(this.$("#username").val(), this.$("#password").val(), {
				success: function(user) {
					// Do stuff after successful login.
					userView.show();
				},
		    	error: function(user, error) {
			    	// The login failed. Check error to see why.
			    	this.$("#signinError").show();
		  		}
		  	});
		}
	},

	logout: function(){
		Parse.User.logOut();
		$("[id*='-wrap']").hide();
		homeView.show();
	},

	show: function(){
		this.render();
	}
});