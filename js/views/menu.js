var app = app || {};

app.MenuView = Backbone.View.extend({

	el: "#navUL",

	events: {
		'click #btnNavHome': 'goHome',
		'click #btnNavAbout': 'goHome',
		'click #btnNavContact': 'goContact',
		'click #btnNavLogin': 'goLogin',
		'click #btnNavLogout': 'goLogout',
		'click #btnNavCreateAccount': 'goCreateAccount'
	},

	initialize: function(){
		
	},

	render: function(){
		var valid = Parse.User.current() ? true : false;
		//this.$("#btnNavHome").toggle(valid);
		//this.$("#btnNavAbout").toggle(valid);
		//this.$("#btnNavContact").toggle(valid);
		this.$("#btnNavLogin").toggle(!valid);
		this.$("#btnNavLogout").toggle(valid);
		this.$("#btnNavCreateAccount").toggle(!valid);
		return this;
	},

	show: function(){
		this.render();
	},

	goHome: function(){
		alert('goHome');
	},

	goContact: function(){
		contactView.show();
	},

	goLogin: function(){
		loginView.show();
	},

	goLogout: function(){
		loginView.logout();
	},

	goCreateAccount: function(){
		accountView.show();
	}
});