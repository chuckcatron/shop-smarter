var app = app || {};

app.AccountView = Backbone.View.extend({

	el: $("#account-wrap"),

	events: {
		'click #btnUpdateAccount': 'create',
	},

	initialize: function(){
		this.render();
	},

	render: function(){
		$("[id*='-wrap']").hide();
		this.$el.show();
	},

	create: function(){
		var validator = this.$el.kendoValidator({
		    					rules: {
		      						match: function(input){
										    	if (input.is("#confirmpassword")) {
										    		return input.val() === $("#accountpassword").val();
										    	}
										    	return true;
											}
	    							},
		    						messages: {
		      								match: "password and confirm password must match"    
		    						}
	  					}).data("kendoValidator");
	    if(validator.validate()){
	    	firstname = $("#firstname").val().trim();
	    	lastname = $("#lastname").val().trim();
	    	email = $("#email").val().trim();
	    	accountpassword = $("#accountpassword").val().trim();

			var user = new Parse.User();
			user.set("username", email);
			user.set("password", accountpassword);
			user.set("email", email);
			 
			// other fields can be set just like with Parse.Object
			user.set("firstname", firstname);
			user.set("lastname", lastname);
			 
			user.signUp(null, {
			  success: function(user) {
			    alert("Your account has been created please login to get started.")
			  	loginView.show();
			  },
			  error: function(user, error) {
			    // Show the error message somewhere and let the user try again.
			    alert("Error: " + error.code + " " + error.message);
			  }
			});
		}
	},

	logout: function(){
		Parse.User.logOut();
		$("[id*='-wrap']").hide();
		homeView.show();
	},

	show: function(){
		this.render();
	}
});