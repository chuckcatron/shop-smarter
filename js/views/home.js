var app = app || {};

app.HomeView = Backbone.View.extend({

	el: $("#home-wrap"),

	initialize: function(){
		this.render();
	},

	render: function(){
		$("[id*='-wrap']").hide();
		this.$el.show();
	},

	show: function(){
		this.render();
	}
});